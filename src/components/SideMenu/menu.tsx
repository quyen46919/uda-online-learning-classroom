import React from 'react'
import {
  BusinessBagIcon,
  CalendarOutlineIcon,
  DashboardIcon,
  DescriptionOutlineIcon,
  InfoIcon,
  PeopleAltOutlineIcon,
  SettingIcon,
} from 'libs/icons'

export const menu = [
  {
    icon: <DashboardIcon />,
    title: 'Dashboard',
    path: '/manager/dashboard',
  },
  {
    icon: <BusinessBagIcon />,
    title: 'Công việc',
    path: '/manager/jobs',
  },
  {
    icon: <PeopleAltOutlineIcon />,
    title: 'Ứng cử viên',
    path: '/manager/candidates',
  },
  {
    icon: <CalendarOutlineIcon />,
    title: 'Lịch',
    path: '/manager/calendar',
  },
  {
    icon: <DescriptionOutlineIcon />,
    title: 'Tài liệu',
    path: '/manager/document',
  },
]

export const subMenu = [
  {
    icon: <SettingIcon />,
    title: 'Cài đặt',
    path: '/manager/setting',
  },
  {
    icon: <InfoIcon />,
    title: 'Hỗ trợ',
    path: '/manager/support',
  },
]

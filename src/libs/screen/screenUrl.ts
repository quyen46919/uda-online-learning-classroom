export const screenUrl = {
  HOME: '/',
  LOGIN: '/login',
  REGISTER: '/register',
  CREATE_POST: '/create-post',
  COURSE_DETAIL: '/course-detail',
  FORUM: '/forum',
  MANAGER_DASHBOARD: '/manager/dashboard',
  MANAGER_CANDIDATES: '/manager/candidates',
  MANAGER_JOBS: '/manager/jobs',
  MEETING: '/meeting',
  TENSORFLOW: '/tensorflow',
}

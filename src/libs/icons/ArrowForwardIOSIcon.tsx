import React from 'react'
import { SvgIcon, SvgIconProps } from '@mui/material'

export const ArrowForwardIOSIcon = (props: SvgIconProps) => {
  return (
    <SvgIcon
      {...props}
      sx={{
        width: '24px',
        height: '24px',
      }}
      viewBox="0 0 24 24"
    >
      <path d="M7.37999 21.01C7.86999 21.5 8.65999 21.5 9.14999 21.01L17.46 12.7C17.85 12.31 17.85 11.68 17.46 11.29L9.14999 2.98005C8.65999 2.49005 7.86999 2.49005 7.37999 2.98005C6.88999 3.47005 6.88999 4.26005 7.37999 4.75005L14.62 12L7.36999 19.25C6.88999 19.73 6.88999 20.5301 7.37999 21.01Z" />
    </SvgIcon>
  )
}

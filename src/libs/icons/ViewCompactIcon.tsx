import React from 'react'
import { SvgIcon, SvgIconProps } from '@mui/material'

export const ViewCompactIcon = (props: SvgIconProps) => {
  return (
    <SvgIcon
      {...props}
      sx={{
        width: '24px',
        height: '24px',
      }}
      viewBox="0 0 24 24"
      fill="none"
    >
      <path
        d="M5 19H9V12H3V17C3 18.1 3.9 19 5 19ZM10 19H20C21.1 19 22 18.1 22 17V12H10V19ZM3 7V11H22V7C22 5.9 21.1 5 20 5H5C3.9 5 3 5.9 3 7Z"
        fill="black"
        fillOpacity="0.54"
      />
    </SvgIcon>
  )
}

import Communication from './communication.jpg'
import Grammar from './grammar.jpg'
import HomeImage from './home.jpg'
import Presentation from './presentation.jpg'
import SlideHome1 from './slide-home1.jpg'
import SlideHome2 from './slide-home2.jpg'
import SlideHome3 from './slide-home3.jpg'
import SlideHome4 from './slide-home4.jpg'
import SlideHome5 from './slide-home5.jpg'
import SlideHome6 from './slide-home6.jpg'
import SlideHome7 from './slide-home7.jpg'
import Writing from './writing.jpg'
import BreakTime from './break-time.png'
import Thumbnail from './thumbnail.jpg'

export {
  Communication,
  Grammar,
  HomeImage,
  Presentation,
  SlideHome1,
  SlideHome2,
  SlideHome3,
  SlideHome4,
  SlideHome5,
  SlideHome6,
  SlideHome7,
  Writing,
  BreakTime,
  Thumbnail,
}

export enum ACTIVATE_STATUS {
  ONLINE = 0,
  OFFLINE = 1,
  DO_NOT_DISTURB = 2,
  AWAY = 3,
}

export enum TAGS {
  NEW = 0,
  BESTSELLER = 1,
  SPECIAL_OFFER = 2,
}

export enum COURSE_CONTENT_ROW {
  VIDEO = 'video',
  DOCUMENT = 'doc',
  ASSIGNMENT = 'assign',
  FILE = 'file',
}

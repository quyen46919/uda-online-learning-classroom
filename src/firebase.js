// Import the functions you need from the SDKs you need
import { initializeApp } from 'firebase/app'
import { getFirestore } from 'firebase/firestore'
import { getDatabase } from 'firebase/database'

const firebaseConfig = {
  apiKey: 'AIzaSyCNCRBruUZBgSPMSppqJSU19Ga2dOkS8Gk',
  authDomain: 'uda-online-classroom.firebaseapp.com',
  projectId: 'uda-online-classroom',
  storageBucket: 'uda-online-classroom.appspot.com',
  messagingSenderId: '386662663992',
  appId: '1:386662663992:web:d89628f55016ae7e05a3cd',
  measurementId: 'G-6294HTH03M',
}

export const app = initializeApp(firebaseConfig)
const db = getFirestore(app)
export const firebaseRealtimeDB = getDatabase()
export default db

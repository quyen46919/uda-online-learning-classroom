import LayoutContainer, { elementWrapper } from 'layouts/LayoutContainer'
import { screenUrl } from 'libs/screen/screenUrl'
import { Route, BrowserRouter as Router, Routes } from 'react-router-dom'
import HomePage from 'pages/home'
import Manager from 'pages/manager'
import Candidates from 'pages/manager/candidates'
import Jobs from 'pages/manager/jobs'
import Post from 'pages/forum'
import CreatePostPage from 'pages/create-post'
import Meeting from 'pages/meeting'
import TensorflowJS from 'pages/tensorflow'

const AppRouter = () => {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<LayoutContainer />}>
          <Route path={screenUrl.HOME} element={elementWrapper(HomePage)} />
          <Route path={screenUrl.MANAGER_DASHBOARD} element={elementWrapper(Manager)} />
          <Route path={screenUrl.MANAGER_CANDIDATES} element={elementWrapper(Candidates)} />
          <Route path={screenUrl.MANAGER_JOBS} element={elementWrapper(Jobs)} />
          <Route path={screenUrl.FORUM} element={elementWrapper(Post)} />
          <Route path={screenUrl.CREATE_POST} element={elementWrapper(CreatePostPage)} />
          <Route path={screenUrl.MEETING} element={elementWrapper(Meeting)} />
          <Route path={screenUrl.TENSORFLOW} element={elementWrapper(TensorflowJS)} />
        </Route>
      </Routes>
    </Router>
  )
}

export default AppRouter

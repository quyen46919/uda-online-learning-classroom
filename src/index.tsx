/* eslint-disable @typescript-eslint/no-explicit-any */
import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './App'
import { applyMiddleware, combineReducers, compose as ReduxCompose, createStore } from 'redux'
import { Actions, reducer, Selectors } from '@andyet/simplewebrtc'
import Thunk from 'redux-thunk'
import { Provider } from 'react-redux'
import commonSlice from 'store/common.slice'

const compose = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || ReduxCompose
const store = createStore(
  combineReducers({ simplewebrtc: reducer, common: commonSlice.reducer }),
  { simplewebrtc: {} as any },
  compose(applyMiddleware(Thunk)),
)

;(window as any).store = store
;(window as any).actions = Actions
;(window as any).selectors = Selectors

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root'),
)

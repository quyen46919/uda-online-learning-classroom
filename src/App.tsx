import React, { useEffect } from 'react'
import './styles/globals.css'
import { CssBaseline, ThemeProvider } from '@mui/material'
import theme from 'themeProvider/ThemeProvider'
import AppRouter from 'router'
import { v4 as uuidv4 } from 'uuid'

function App() {
  useEffect(() => {
    const savedUUid = localStorage.getItem('UDAUserId')
    if (!savedUUid) {
      localStorage.setItem('UDAUserId', uuidv4())
    }
  }, [])
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <AppRouter />
    </ThemeProvider>
  )
}

export default App

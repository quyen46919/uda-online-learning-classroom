import { createSlice, PayloadAction } from '@reduxjs/toolkit'
interface CommonState {
  userBehavior: string
  userId: string | null
  currentRoomName: string
}

const initialState: CommonState = {
  userId: localStorage.getItem('UDAUserId'),
  userBehavior: '',
  currentRoomName: 'UDA',
}

const commonSlice = createSlice({
  name: 'common',
  initialState,
  reducers: {
    setUserBehavior(state, action: PayloadAction<string>) {
      state.userBehavior = action.payload
    },
    setCurrentRoomName(state, action: PayloadAction<string>) {
      state.currentRoomName = action.payload
    },
  },
})

export const { setUserBehavior, setCurrentRoomName } = commonSlice.actions
export default commonSlice

/* eslint-disable @typescript-eslint/no-explicit-any */
export const getUserBehavior = (state: any) => state.common.userBehavior
export const getRoomName = (state: any) => state.common.currentRoomName

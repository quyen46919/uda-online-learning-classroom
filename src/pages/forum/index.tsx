import React from 'react'
import { Stack, useMediaQuery, useTheme } from '@mui/material'
import Tags from 'components/Tags'
import {
  FollowingIcon,
  NewIcon,
  PopularIcon,
  JSIcon,
  BitcoinIcon,
  DesignIcon,
  BusinessIcon,
  InnovationIcon,
  TutorialIcon,
} from 'libs/icons'
import SchedulingTags from 'components/Tags/SchedulingTags'
import PodcastTags from 'components/Tags/PodcastTags'
import Question from 'components/Question'
import { questionsForum } from 'seed_data'
import ActionTags from 'components/Tags/ActionTags'

const listActionItems = {
  title: 'Mau đến gửi bài nào',
  data: [
    {
      icon: <NewIcon />,
      primary: 'Câu hỏi',
      href: '/create-post',
    },
    {
      icon: <NewIcon />,
      primary: 'Bài đăng',
      href: '/create-post',
    },
    {
      icon: <NewIcon />,
      primary: 'Sự kiện',
      href: '/create-post',
    },
  ],
}

const items = {
  data: [
    {
      icon: <NewIcon />,
      primary: 'Newest and Recent',
      primaryMobile: 'Newest',
      secondary: 'Find the latest update',
    },
    {
      icon: <PopularIcon />,
      primary: 'Popular of the day',
      primaryMobile: 'Popular',
      secondary: 'Short featured today by curators',
    },
    {
      icon: <FollowingIcon />,
      primary: 'Following',
      primaryMobile: 'Following',
      notificationNumber: 100,
      secondary: 'Explore from your favorite person',
    },
  ],
}

const popularItems = {
  title: 'Popular Tags',
  data: [
    {
      icon: <JSIcon />,
      primary: '#javascript',
      secondary: '82,645 Posted by this tag',
    },
    {
      icon: <BitcoinIcon />,
      primary: '#bitcoin',
      secondary: '65,523 Posted • Trending',
    },
    {
      icon: <DesignIcon />,
      primary: '#design',
      secondary: '51,354 • Trending in Bangladesh',
    },
    {
      icon: <InnovationIcon />,
      primary: '#innovation',
      secondary: '48,029 Posted by this tag',
    },
    {
      icon: <TutorialIcon />,
      primary: '#tutorial',
      secondary: '51,354 • Trending in Bangladesh',
    },
    {
      icon: <BusinessIcon />,
      primary: '#busieness',
      secondary: '82,645 Posted by this tag',
    },
  ],
}

const schedulingItems = {
  title: 'Meetups',
  href: '/',
  data: [
    {
      primary: 'UIHUT - Crunchbase Company Profile UIHUT',
      secondary: 'UIHUT  •  Sylhet, Bangladesh UIHUT',
      avatar: 'https://vtv1.mediacdn.vn/thumb_w/650/2022/4/26/1000-1650949519392431771941.jpeg',
      tags: [{ label: 'Remote' }, { label: 'Part-time' }, { label: 'Worldwide' }],
    },
    {
      primary: 'Design Meetups USA | Dribbble',
      secondary: 'Dribbble  •  Austin, Texas, USA',
      avatar: 'https://vtv1.mediacdn.vn/thumb_w/650/2022/4/26/1000-1650949519392431771941.jpeg',
      tags: [{ label: 'Remote' }, { label: 'Part-time' }],
    },
    {
      primary: 'Meetup Brand Identity Design',
      secondary: 'Behance  •  Sab jose, Califonia, USA',
      avatar: 'https://vtv1.mediacdn.vn/thumb_w/650/2022/4/26/1000-1650949519392431771941.jpeg',
      tags: [{ label: 'Full Time' }, { label: 'Contract' }, { label: 'Worldwide' }],
    },
  ],
}

const podcastItems = {
  title: 'Podcasts',
  href: '/',
  data: [
    {
      img: 'https://img.freepik.com/free-vector/blockchain-technology-security-template-vector-data-payment-securing-blog-banner_53876-112174.jpg?w=1380&t=st=1672749768~exp=1672750368~hmac=81aea0c926b3e21e940e8568eda5c6dda9665e0dc1b682cf6d71519847f3fa2f',
      primary: 'Selling a Business and Scaling Another Amidst Tragedy.',
      secondary: 'by Michele Hansen',
    },
    {
      img: 'https://img.freepik.com/free-vector/blockchain-technology-security-template-vector-data-payment-securing-blog-banner_53876-112174.jpg?w=1380&t=st=1672749768~exp=1672750368~hmac=81aea0c926b3e21e940e8568eda5c6dda9665e0dc1b682cf6d71519847f3fa2f',
      primary: 'Mental health as a founder and the importance of community',
      secondary: 'by James McKinven',
    },
    {
      img: 'https://img.freepik.com/free-vector/blockchain-technology-security-template-vector-data-payment-securing-blog-banner_53876-112174.jpg?w=1380&t=st=1672749768~exp=1672750368~hmac=81aea0c926b3e21e940e8568eda5c6dda9665e0dc1b682cf6d71519847f3fa2f',
      primary: 'Growing to $8.5k MRR in 1 year - Marie Martens, Tally.so',
      secondary: 'by Mahfuzul Nabil',
    },
    {
      img: 'https://img.freepik.com/free-vector/blockchain-technology-security-template-vector-data-payment-securing-blog-banner_53876-112174.jpg?w=1380&t=st=1672749768~exp=1672750368~hmac=81aea0c926b3e21e940e8568eda5c6dda9665e0dc1b682cf6d71519847f3fa2f',
      primary: 'Mental Health and Bootstrapping in 2022 with Rob Walling of TinySe',
      secondary: 'by Dr. Jubed',
    },
    {
      img: 'https://img.freepik.com/free-vector/blockchain-technology-security-template-vector-data-payment-securing-blog-banner_53876-112174.jpg?w=1380&t=st=1672749768~exp=1672750368~hmac=81aea0c926b3e21e940e8568eda5c6dda9665e0dc1b682cf6d71519847f3fa2f',
      primary: 'Money, Happiness, and Productivity as a Solo Founder with Pieter Levels',
      secondary: 'by Jesse Hanley',
    },
    {
      img: 'https://img.freepik.com/free-vector/blockchain-technology-security-template-vector-data-payment-securing-blog-banner_53876-112174.jpg?w=1380&t=st=1672749768~exp=1672750368~hmac=81aea0c926b3e21e940e8568eda5c6dda9665e0dc1b682cf6d71519847f3fa2f',
      primary: 'Mental health as a founder and the importance of community',
      secondary: 'by Courtland Allen',
    },
  ],
}

const Post = () => {
  const theme = useTheme()
  const downSm = useMediaQuery(theme.breakpoints.down('sm'))

  return (
    <Stack
      sx={{ backgroundColor: theme.palette.secondary.main }}
      direction={{ xs: 'column', lg: 'row' }}
      minHeight="calc(100vh - 71px)"
      p={{ xs: '20px', lg: '20px 40px 0 40px' }}
      gap="20px"
      maxWidth={{ lg: '1536px', xl: '1920px' }}
    >
      <Stack
        minWidth={{ lg: '240px' }}
        sx={{
          position: { lg: 'sticky' },
          top: { lg: '90px' },
          height: { lg: 'calc(100vh - 110px)' },
          overflowY: 'scroll',
          '::-webkit-scrollbar': {
            display: 'none',
          },
          msOverflowStyle: 'none' /* Hide Scrollbar IE and Edge */,
          scrollbarWidth: 'none',
        }}
        borderRadius="16px"
        gap="20px"
      >
        <ActionTags items={listActionItems} />
        <Tags items={items} />
        {!downSm && <Tags items={popularItems} />}
      </Stack>
      <Stack flex="1" borderRadius="16px" gap={{ xs: 0, lg: '20px' }} marginBottom={{ lg: '20px' }}>
        <Question data={questionsForum} />
      </Stack>
      <Stack
        minWidth={{ lg: '325px' }}
        sx={{
          position: { lg: 'sticky' },
          top: { lg: '90px' },
          height: { lg: 'calc(100vh - 110px)' },
          overflowY: 'scroll',
          '::-webkit-scrollbar': {
            display: 'none',
          },
          msOverflowStyle: 'none',
          scrollbarWidth: 'none',
        }}
        mb={{ xs: '68px', sm: 0, lg: 0 }}
        borderRadius="16px"
        gap="20px"
      >
        <SchedulingTags items={schedulingItems} />
        <PodcastTags items={podcastItems} />
      </Stack>
    </Stack>
  )
}

Post.PageProps = {
  title: 'Diễn đàn',
  showHeader: true,
}

export default Post

import { Box, Button } from '@mui/material'
import { useCallback, useEffect, useRef, useState } from 'react'
import * as ml5 from 'ml5'
import { useDispatch } from 'react-redux'
import { setUserBehavior } from 'store/common.slice'
import { useSelector } from 'react-redux'
import { getRoomName } from 'store/common.selector'
import { firebaseRealtimeDB } from '../../firebase'
import { push, ref } from 'firebase/database'

const checkResult = (label) =>
  ['Học nghiêm túc', 'Đọc tài liệu', 'Hành động khác'].includes(label) ? true : false

const RecognitionProvider = () => {
  const videoRef = useRef()
  const [gestureLabel, setGestureLabel] = useState('LOADING.....')
  const [isShow, setIsShow] = useState(true)
  const gestureClassifier = useRef()
  const timeOut = useRef()
  const dispatch = useDispatch()
  const roomName = useSelector(getRoomName)
  const userId = localStorage.getItem('UDAUserId')

  const load = useCallback(() => {
    // gesture recognition
    const setupCamera = async () => {
      if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
        const stream = await navigator.mediaDevices.getUserMedia({ video: true })
        if (videoRef.current) {
          videoRef.current.srcObject = stream
          setGestureLabel('MODEL LOADING...')
        }
      }
    }

    const loadGestureClassifier = async () => {
      const gestureModel = 'https://teachablemachine.withgoogle.com/models/rEwRJdP_y/model.json'
      const abc = ml5.imageClassifier(gestureModel, () => {
        console.log('model loaded!')
      })
      gestureClassifier.current = abc
    }

    async function classifyImage() {
      if (gestureClassifier.current && videoRef.current) {
        await gestureClassifier.current.classify(videoRef.current, gotGestureResult)
      }
      timeOut.current = setTimeout(classifyImage, 2000)
    }

    const gotGestureResult = (err, rs) => {
      if (err) {
        console.error(err)
        return
      }
      const maxConfidence = Math.max(...rs.map((o) => o.confidence))
      const top1 = rs?.find((result) => result.confidence === maxConfidence)
      sessionStorage.setItem('currentUserBehavior', top1.label)
      dispatch(setUserBehavior(top1.label))
      if (top1.label) {
        push(ref(firebaseRealtimeDB, `userBehaviors/${roomName}/${userId}/`), {
          ...rs,
          time: Date.now(),
          result: checkResult(top1.label),
        })
      }
      setGestureLabel(top1.label)
    }

    setupCamera()
    loadGestureClassifier()
    classifyImage()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [roomName])

  useEffect(() => {
    if (isShow) {
      load()
    } else {
      clearTimeout(timeOut.current)
    }
  }, [isShow, load])

  return (
    <Box display="flex" gap="20px">
      {/* <Stack sx={{ '& *': { visibility: 'hidden', opacity: '0' } }}>
        <Typography mt="20px" ml="30px">
          {gestureLabel}
        </Typography>
      </Stack> */}
      <video ref={videoRef} width="600" height="400" hidden="true" autoPlay="true" />
      {isShow && (
        <Button variant="contained" onClick={() => setIsShow(!isShow)}>
          {gestureLabel}
        </Button>
      )}
      <Button variant="contained" onClick={() => setIsShow(!isShow)}>
        {isShow ? 'Tắt nhận diện' : 'Bật nhận diện'}
      </Button>
    </Box>
  )
}

export default RecognitionProvider

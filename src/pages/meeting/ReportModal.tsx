/* eslint-disable @typescript-eslint/no-explicit-any */
import * as React from 'react'
import Button from '@mui/material/Button'
import { styled } from '@mui/material/styles'
import Dialog from '@mui/material/Dialog'
import DialogTitle from '@mui/material/DialogTitle'
import DialogContent from '@mui/material/DialogContent'
import DialogActions from '@mui/material/DialogActions'
import IconButton from '@mui/material/IconButton'
import CloseIcon from '@mui/icons-material/Close'
import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  ResponsiveContainer,
  Tooltip,
  Legend,
} from 'recharts'

import { useNavigate } from 'react-router-dom'
import { Record } from './LocalMediaControls'

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  '& .MuiDialogContent-root': {
    padding: theme.spacing(2),
  },
  '& .MuiDialogActions-root': {
    padding: theme.spacing(1),
  },
}))

export interface DialogTitleProps {
  id: string
  children?: React.ReactNode
  onClose: () => void
}

function BootstrapDialogTitle(props: DialogTitleProps) {
  const { children, onClose, ...other } = props

  return (
    <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
      {children}
      {onClose ? (
        <IconButton
          aria-label="close"
          onClick={onClose}
          sx={{
            position: 'absolute',
            right: 8,
            top: 8,
            color: (theme) => theme.palette.grey[500],
          }}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </DialogTitle>
  )
}
interface CustomizedDialogProps {
  open: boolean
  handleClose: () => void
  chartData: Record[] | undefined
}

export default function CustomizedDialogs({ open, handleClose, chartData }: CustomizedDialogProps) {
  const navigate = useNavigate()
  let counter = 0
  chartData?.forEach((item: any) => {
    if (item.result) counter++
  })
  return (
    <div>
      <BootstrapDialog onClose={handleClose} aria-labelledby="customized-dialog-title" open={open}>
        <BootstrapDialogTitle id="customized-dialog-title" onClose={handleClose}>
          Kết quả quá trình học tập của bạn:{' '}
          {chartData && counter >= 0.7 * chartData.length ? 'Đạt' : 'Không đạt'}
        </BootstrapDialogTitle>
        <DialogContent dividers>
          <ResponsiveContainer height={400} width={500}>
            <LineChart data={chartData} margin={{ right: 25, top: 10 }}>
              <CartesianGrid strokeDasharray="3 3" />
              <XAxis dataKey="time" interval="preserveEnd" />
              <YAxis interval="preserveEnd" />
              <Tooltip />
              <Legend />
              <Line type="monotone" dataKey="ngủ" stroke="#E74C3C" />
              <Line type="monotone" dataKey="Hành động khác" stroke="#85C1E9" />
              <Line type="monotone" dataKey="Học nghiêm túc" stroke="#27AE60" />
              <Line type="monotone" dataKey="Rời khỏi chỗ" stroke="#D68910" />
              <Line type="monotone" dataKey="Đọc tài liệu" stroke="#839192" />
              <Line type="monotone" dataKey="Bấm điện thoại" stroke="#66122" />
            </LineChart>
          </ResponsiveContainer>
        </DialogContent>
        <DialogActions>
          <Button autoFocus onClick={() => navigate('/forum')}>
            Về trang chủ
          </Button>
        </DialogActions>
      </BootstrapDialog>
    </div>
  )
}

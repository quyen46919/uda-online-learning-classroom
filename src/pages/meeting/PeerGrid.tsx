import { PeerList, RemoteMediaList } from '@andyet/simplewebrtc'
import { Box } from '@mui/material'
import HiddenPeers from 'contexts/HiddenPeers'
import { useContext } from 'react'
import PeerGridItem from './PeerGridItem'

interface PeerGridProps {
  roomAddress: string
  activeSpeakerView: boolean
  setPassword?: (password: string) => void
}

// PeerGrid is the main video display for Talky. It matches remoteMedia to
// peers and then renders a PeerGridItem for each peer in the room.
const PeerGrid = ({ roomAddress, activeSpeakerView, setPassword }: PeerGridProps) => {
  const { hiddenPeers } = useContext(HiddenPeers)
  return (
    <PeerList
      speaking={activeSpeakerView ? activeSpeakerView : undefined}
      room={roomAddress}
      render={({ peers }) => {
        const visiblePeers = peers.filter((p) => !hiddenPeers.includes(p.id))
        if (visiblePeers.length > 0 || activeSpeakerView) {
          return (
            <>
              {visiblePeers.map((peer) => (
                <Box key={peer.id}>
                  <RemoteMediaList
                    peer={peer.address}
                    render={({ media }) => {
                      return (
                        <Box>
                          <PeerGridItem
                            media={media}
                            peer={peer}
                            setPassword={setPassword}
                            onlyVisible={visiblePeers.length === 1}
                            roomAddress={roomAddress}
                          />
                        </Box>
                      )
                    }}
                  />
                </Box>
              ))}
            </>
          )
        } else {
          return <span></span>
        }
      }}
    />
  )
}

export default PeerGrid

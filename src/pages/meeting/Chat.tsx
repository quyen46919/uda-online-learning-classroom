import {
  Chat,
  ChatComposers,
  ChatInput,
  ChatInputTextArea,
  ChatList,
  Peer,
  State,
  StayDownContainer,
} from '@andyet/simplewebrtc'
import React, { useEffect, useState } from 'react'
import { CloseIcon, SendIcon } from 'libs/icons'
import { IconButton, Stack, Typography, styled } from '@mui/material'
import { useSelector } from 'react-redux'

type handleDrawerToggle = () => void

interface Props {
  handleDrawerToggle: handleDrawerToggle
}

interface ChatMessageGroupProps {
  chats: Chat[]
  peer: Peer | undefined
}

const StyledStayDownContainer = styled(StayDownContainer)({
  flex: 1,
  overflow: 'scroll',
  height: '0px',
  marginBottom: '16px',
  '::-webkit-scrollbar': {
    display: 'none',
  },
  msOverflowStyle: 'none' /* Hide Scrollbar IE and Edge */,
  scrollbarWidth: 'none',
})

const StyledTextfield = styled(ChatInputTextArea)({
  resize: 'none',
  width: '100%',
  padding: '6px',
})

const ChatMessageGroup = ({ chats, peer }: ChatMessageGroupProps) => (
  <Stack key={chats[0].id} p="12px 0">
    <Stack direction="row" gap="6px">
      <Typography component="span" fontWeight="bold">
        {chats[0].displayName ? chats[0].displayName : 'Anonymous'}
      </Typography>
      <Typography component="span">{chats[0].time.toLocaleTimeString()}</Typography>
    </Stack>
    {chats.map((message) => (
      <Stack key={message.id}>
        <Typography
          component="span"
          display="inline-block"
          sx={{
            wordWrap: 'break-word',
          }}
        >
          {message.body}
        </Typography>
      </Stack>
    ))}
  </Stack>
)

// ChatContainer renders all the UI for the chat room inside a Room. It
// includes a message display embedded inside a StayDownContainer so that
// it remains scrolled to the bottom, a ChatInput to type messages, and a
// text element that displays currently typing peers.
const ChatContainer = (props: Props) => {
  const { handleDrawerToggle } = props
  const myState = useSelector((state: State) => state.simplewebrtc)
  const [roomAddress, setRoomAddress] = useState<string>('')
  const [joined, setJoined] = useState<boolean>(false)

  useEffect(() => {
    if (myState) {
      const room = Object.values(myState.rooms)[0]
      if (room) {
        setRoomAddress(room.address)
        setJoined(room.joined)
      }
    }
  }, [myState])

  return (
    <Stack height="100%" justifyContent="space-between">
      <Stack direction="row" alignItems="center" justifyContent="space-between" mb="20px">
        <Typography variant="h2" fontSize="26px" lineHeight="38px">
          Nhắn tin
        </Typography>
        <IconButton onClick={handleDrawerToggle}>
          <CloseIcon />
        </IconButton>
      </Stack>
      <StyledStayDownContainer>
        <ChatList
          room={roomAddress}
          renderGroup={({ chats, peer }) => (
            <ChatMessageGroup key={chats[0].id} chats={chats} peer={peer} />
          )}
        />
      </StyledStayDownContainer>
      <Stack>
        <ChatInput
          autoFocus
          disabled={!joined}
          room={roomAddress}
          placeholder={joined ? 'Gửi tin nhắn cho mọi người' : 'Đang chờ tham gia phòng họp'}
          render={(chatProps) => (
            <Stack
              direction="row"
              gap="5px"
              sx={{
                '& input:focus': {
                  borderColor: 'blue',
                },
              }}
            >
              <StyledTextfield {...chatProps} />
              <IconButton color="primary" onClick={chatProps.sendMessage}>
                <SendIcon />
              </IconButton>
            </Stack>
          )}
        />
      </Stack>
      <div>
        <ChatComposers room={roomAddress} />
      </div>
    </Stack>
  )
}

export default ChatContainer

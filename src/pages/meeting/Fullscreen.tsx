/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useRef, useState } from 'react'

export interface FullScreenRenderProps {
  fullScreenActive: boolean
  toggleFullScreen: () => Promise<void>
}

export interface FullScreenProps {
  id?: string
  className?: string
  style?: React.CSSProperties
  render?: (props: FullScreenRenderProps) => React.ReactNode
  children?: React.ReactNode | ((props: FullScreenRenderProps) => React.ReactNode)
}

const Fullscreen: React.FC<FullScreenProps> = (props) => {
  const elRef = useRef<any>(null)
  const [fullScreenActive, setFullScreenActive] = useState(false)

  async function startFullScreen(): Promise<void> {
    const el = elRef.current
    const fn =
      el?.requestFullscreen ||
      el?.msRequestFullscreen ||
      el?.mozRequestFullScreen ||
      el?.mozRequestFullscreen ||
      el?.webkitRequestFullScreen ||
      el?.webkitRequestFullscreen

    if (fn) {
      const res = await fn.call(el)
      setFullScreenActive(true)
      return res
    }
  }

  async function exitFullScreen(): Promise<void> {
    const fn =
      document.exitFullscreen ||
      (document as any).mozCancelFullScreen ||
      (document as any).webkitExitFullscreen

    if (fn) {
      const res = await fn.call(document)
      setFullScreenActive(false)
      return res
    }
  }

  function fullScreenActiveFn(): boolean {
    const fullScreenEl =
      document.fullscreenElement ||
      (document as any).mozFullScreenElement ||
      (document as any).webkitFullscreenElement

    return !!fullScreenEl
  }

  async function toggleFullScreen(): Promise<void> {
    if (fullScreenActiveFn()) {
      return exitFullScreen()
    } else {
      return startFullScreen()
    }
  }

  const renderContent: any = () => {
    const renderProps: FullScreenRenderProps = {
      fullScreenActive: fullScreenActiveFn(),
      toggleFullScreen: () => toggleFullScreen(),
    }

    let render = props.render
    if (!render && typeof props.children === 'function') {
      render = props.children as (props: FullScreenRenderProps) => React.ReactNode
    }
    return render ? render(renderProps) : props.children
  }

  return (
    <div id={props.id} className={props.className} style={props.style} ref={elRef}>
      {renderContent()}
    </div>
  )
}

export default Fullscreen

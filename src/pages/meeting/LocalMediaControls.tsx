/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useState } from 'react'
import { RequestUserMedia } from '@andyet/simplewebrtc'
import { Button, IconButton, Stack } from '@mui/material'
import { MicIcon, MicOffIcon, VideoCamIcon, VideoCamOffIcon } from 'libs/icons'
import ScreenShareControls from './ScreenshareControls'
import { firebaseRealtimeDB } from '../../firebase'
import { get, ref } from 'firebase/database'
import CustomizedDialogs from './ReportModal'

interface LocalMediaControlsProps {
  hasAudio: boolean
  isMuted: boolean
  unmute: () => void
  mute: () => void
  isPaused: boolean
  isSpeaking: boolean
  isSpeakingWhileMuted: boolean
  resumeVideo: () => void
  pauseVideo: () => void
}

export interface RecordBase {
  behavior: string
  time: number
}

export interface Record {
  'Rời khỏi chỗ': string
  Ngủ: string
  'Đọc tài liệu': string
  'Hành động khác': string
  'Bấm điện thoại': string
  'Học nghiêm túc': string
  result: boolean
  time: string
}

const colorList = ['#E74C3C', '#85C1E9', '#27AE60', '#D68910', '#839192', '#273746']
const labelList = [
  'Học nghiêm túc',
  'Đọc tài liệu',
  'ngủ',
  'Bấm điện thoại',
  'Rời khỏi chỗ',
  'Hành động khác',
]

const data = [
  {
    name: 'Page A',
    uv: 4000,
    pv: 2400,
    amt: 2400,
  },
  {
    name: 'Page B',
    uv: 3000,
    pv: 1398,
    amt: 2210,
  },
  {
    name: 'Page C',
    uv: 2000,
    pv: 9800,
    amt: 2290,
  },
  {
    name: 'Page D',
    uv: 2780,
    pv: 3908,
    amt: 2000,
  },
  {
    name: 'Page E',
    uv: 1890,
    pv: 4800,
    amt: 2181,
  },
  {
    name: 'Page F',
    uv: 2390,
    pv: 3800,
    amt: 2500,
  },
  {
    name: 'Page G',
    uv: 3490,
    pv: 4300,
    amt: 2100,
  },
]

function formatTimestamp(timestamp: number) {
  const date = new Date(timestamp)
  const hours = date.getHours()
  const minutes = date.getMinutes()
  const seconds = date.getSeconds()

  const formattedTime = `${hours < 10 ? '0' + hours : hours}:${
    minutes < 10 ? '0' + minutes : minutes
  }:${seconds < 10 ? '0' + seconds : seconds}`

  return formattedTime
}

function formatDecimal(number: number) {
  const roundedNumber = Number(number.toFixed(3))
  return roundedNumber.toString()
}

// LocalMediaControls displays buttons to toggle the mute/pause state of the
// user's audio/video.
const LocalMediaControls = ({
  hasAudio,
  isMuted,
  unmute,
  mute,
  isPaused,
  isSpeakingWhileMuted,
  resumeVideo,
  pauseVideo,
}: LocalMediaControlsProps) => {
  const [openModal, setOpenModal] = useState<boolean>(false)
  const [chartData, setChartData] = useState<Record[]>()
  const handleEndMeeting = () => {
    const userId = localStorage.getItem('UDAUserId')
    const behaviorsRef = ref(firebaseRealtimeDB, `userBehaviors/UDA/${userId}/`)
    get(behaviorsRef)
      .then((snapshot) => {
        const behaviors: any = []
        snapshot.forEach((childSnapshot) => {
          behaviors.push(childSnapshot.val())
        })
        // Tạo danh sách các hành động khác nhau trong khoảng thời gian quy định
        setOpenModal(true)
        setChartData(
          behaviors.map((record: any) => ({
            'Rời khỏi chỗ': formatDecimal(record[0].confidence),
            Ngủ: formatDecimal(record[1].confidence),
            'Đọc tài liệu': formatDecimal(record[2].confidence),
            'Hành động khác': formatDecimal(record[3].confidence),
            'Bấm điện thoại': formatDecimal(record[4].confidence),
            'Học nghiêm túc': formatDecimal(record[5].confidence),
            result: record.result,
            time: formatTimestamp(record.time),
          })),
        )
      })
      .catch((error) => {
        console.error('Error getting records:', error)
      })
  }

  return (
    <Stack direction="row" alignItems="center" justifyContent="center" gap="20px">
      <RequestUserMedia
        audio={{
          deviceId: {
            ideal: localStorage.preferredAudioDeviceId,
          },
        }}
        share={true}
        render={(getMedia, captureState) => (
          <IconButton
            color={isMuted ? 'white' : 'lightGrey'}
            onClick={() => {
              if (captureState.requestingCapture) {
                return
              } else if (!hasAudio) {
                getMedia()
              } else if (isMuted) {
                unmute()
              } else {
                mute()
              }
            }}
          >
            {isMuted ? <MicOffIcon /> : <MicIcon />}
          </IconButton>
        )}
      />
      <IconButton
        color={isPaused ? 'white' : 'lightGrey'}
        onClick={() => (isPaused ? resumeVideo() : pauseVideo())}
      >
        {isPaused ? <VideoCamOffIcon /> : <VideoCamIcon />}
      </IconButton>
      <Button variant="outlined" onClick={handleEndMeeting}>
        Kết thúc
      </Button>
      <ScreenShareControls />

      {openModal && (
        <CustomizedDialogs
          open={openModal}
          handleClose={() => setOpenModal(false)}
          chartData={chartData}
        />
      )}
    </Stack>
  )
}

export default LocalMediaControls

import { Media, Peer, PeerControls, Video } from '@andyet/simplewebrtc'
import { IconButton, Stack, Typography, styled, useTheme } from '@mui/material'
import { FullscreenIcon, MicIcon, MicOffIcon, PeopleAltIcon, RemoveCircleIcon } from 'libs/icons'
import Fullscreen from './Fullscreen'
interface LoadingVideoProps {
  media: Media
  qualityProfile?: 'high' | 'medium' | 'low'
}

interface PeerGridItemMediaProps {
  media: Media[]
  fullScreenActive?: boolean
}

interface PeerGridItemOverlayProps {
  peer: Peer
  audioIsMuted: boolean
  fullScreenActive: boolean
  toggleFullScreen: () => Promise<void>
  setPassword?: (password: string) => void
  roomAddress: string
}

interface PeerGridItemProps {
  peer: Peer
  media: Media[]
  setPassword?: (password: string) => void
  onlyVisible: boolean
  roomAddress: string
}

const StyledVideo = styled(Video)({
  transform: 'none !important',
  width: '100%',
  height: '100%',
  borderRadius: '25px',
  objectFit: 'cover',
})

const LoadingVideo = (props: LoadingVideoProps) => {
  if (!props.media.loaded) {
    return <span>AudioOnlyPeer</span>
  }
  return (
    <StyledVideo
      media={props.media}
      qualityProfile={props.media.screenCapture ? undefined : props.qualityProfile}
    />
  )
}

// PeerGridItemMedia renders a different visualization based on what media is
// available from a peer. It will render video if the peer is sending video,
// otherwise it renders an audio-only display.
const PeerGridItemMedia = ({ media, fullScreenActive }: PeerGridItemMediaProps) => {
  const videoStreams = media.filter((m) => m.kind === 'video' && !m.remoteDisabled)
  if (videoStreams.length > 0) {
    const webcamStreams = videoStreams.filter((s) => !s.screenCapture)
    const screenCaptureStreams = videoStreams.filter((s) => s.screenCapture)

    if (videoStreams.length === 1) {
      return (
        <LoadingVideo media={videoStreams[0]} qualityProfile={fullScreenActive ? 'high' : 'high'} />
      )
    }
    if (screenCaptureStreams.length === 0) {
      return (
        <LoadingVideo
          media={webcamStreams[0]}
          qualityProfile={fullScreenActive ? 'high' : 'high'}
        />
      )
    }

    return <LoadingVideo media={webcamStreams[0]} qualityProfile="high" />
  }

  return <h1>Audio only peer</h1>
}

function allAudioIsUnmuted(media: Media[]): boolean {
  for (const m of media) {
    if (m.kind === 'audio' && m.remoteDisabled) {
      return false
    }
  }
  return true
}

const PeerGridItemOverlay = ({
  audioIsMuted,
  fullScreenActive,
  peer,
  toggleFullScreen,
  setPassword,
  roomAddress,
}: PeerGridItemOverlayProps) => {
  const theme = useTheme()
  return (
    <Stack position="absolute" top={0} left={0} zIndex={10} width="100%" height="100%" p="15px">
      {/* <div>{peer.rtt && <span>{peer.rtt}</span>}</div> */}
      <PeerControls
        peer={peer}
        render={({ isMuted, mute, unmute, kick }) => {
          return (
            <Stack justifyContent="space-between" height="100%">
              <Stack direction="row" justifyContent="space-between">
                <Stack
                  direction="row"
                  alignItems="center"
                  gap="10px"
                  width="fit-content"
                  p="8px"
                  borderRadius="15px"
                  sx={{
                    backgroundColor: 'rgb(200 196 196 / 30%)',
                    color: theme.palette.white.main,
                  }}
                >
                  <PeopleAltIcon />
                  <Typography component="span">{peer.displayName}</Typography>
                </Stack>
                <IconButton
                  color={isMuted ? 'lightGrey' : 'darkGrey'}
                  onClick={() => (isMuted ? unmute() : mute())}
                >
                  {isMuted ? <MicOffIcon /> : <MicIcon />}
                </IconButton>
              </Stack>
              <Stack direction="row" gap="10px" justifyContent="flex-end">
                <IconButton color="lightGrey" onClick={toggleFullScreen}>
                  <FullscreenIcon />
                </IconButton>
                <IconButton
                  color="lightGrey"
                  onClick={() => {
                    kick()
                  }}
                >
                  <RemoveCircleIcon />
                </IconButton>
              </Stack>
            </Stack>
          )
        }}
      />
    </Stack>
  )
}

// PeerGridItem renders various controls over a peer's media.
const PeerGridItem = ({
  peer,
  media,
  setPassword,
  onlyVisible,
  roomAddress,
}: PeerGridItemProps) => (
  <>
    <Fullscreen
      style={{
        width: '100%',
        height: 'fit-content',
        position: 'relative',
      }}
    >
      {({ fullScreenActive, toggleFullScreen }) => (
        <>
          <PeerGridItemOverlay
            peer={peer}
            fullScreenActive={fullScreenActive}
            audioIsMuted={!allAudioIsUnmuted(media)}
            toggleFullScreen={toggleFullScreen}
            setPassword={setPassword}
            roomAddress={roomAddress}
          />
          <PeerGridItemMedia media={media} fullScreenActive={fullScreenActive || onlyVisible} />
        </>
      )}
    </Fullscreen>
  </>
)

export default PeerGridItem

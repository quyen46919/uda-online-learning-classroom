/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useEffect, ChangeEvent, useState, useCallback } from 'react'
import {
  AddIcon,
  FiberManualRecordIcon,
  MessageIcon,
  PeopleAltIcon,
  StopIcon,
  ViewAgendaIcon,
  ViewCompactIcon,
  ViewModuleIcon,
  VisibilityIcon,
  VisibilityOffIcon,
} from 'libs/icons'
import {
  Button,
  IconButton,
  Stack,
  TextField,
  Typography,
  useMediaQuery,
  useTheme,
} from '@mui/material'
import { TabletLaptopDrawer } from 'components/Drawer'
import GroupMeeting from './GroupMeeting'
import { UserControls } from '@andyet/simplewebrtc'
import LocalMediaControls from './LocalMediaControls'
import ChatContainer from './Chat'
import { useSelector } from 'react-redux'
import { getRoomName } from 'store/common.selector'
import RecognitionProvider from 'pages/tensorflow'

const drawerWidth = 320

const Meeting = () => {
  const theme = useTheme()
  const downLg = useMediaQuery(theme.breakpoints.down('lg'))
  const [drawerOpen, seDrawerOpen] = useState<boolean>(false)
  const [record, setRecord] = useState<boolean>(false)
  const [layout, setLayout] = useState<number>(1)
  const [showMeetingHeader, setShowMeetingHeader] = useState<boolean>(true)
  const [value, setValue] = useState<string>('')
  const roomName =
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    useSelector(getRoomName) || 'UDA'

  const handleToggle = () => {
    seDrawerOpen(!drawerOpen)
  }

  const handleChangeLayout = (layoutNumber: number) => {
    setLayout(layoutNumber)
  }

  const drawer = <ChatContainer handleDrawerToggle={handleToggle} />

  const BackBtn = () => {
    return <Button onClick={handleToggle} variant="outlined" startIcon={<MessageIcon />} />
  }

  const RecordBtn = () => {
    return (
      <IconButton
        onClick={() => setRecord(!record)}
        color="white"
        sx={{
          '& svg path': {
            fill: theme.palette.warning.dark,
          },
        }}
      >
        {record ? <StopIcon /> : <FiberManualRecordIcon />}
      </IconButton>
    )
  }

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => setValue(e.target.value)
  const handleClick = () => {}

  return (
    <Stack bgcolor={theme.palette.primary.main}>
      <Stack
        component="main"
        bgcolor={theme.palette.primary.main}
        sx={{
          p: '20px 24px',
          minHeight: 'calc(100vh - 70px)',
          width: {
            xs: '100%',
            sm: `calc(100% - ${drawerWidth}px)`,
            lg: drawerOpen ? `calc(100% - ${drawerWidth}px)` : '100%',
          },
        }}
      >
        {showMeetingHeader && (
          <Stack direction="row" alignItems="center" justifyContent="space-between" mb="18px">
            <Stack direction="row" alignItems="center" gap="20px">
              <BackBtn />
              <Stack>
                <Typography variant="h1" sx={{ fontSize: '22px' }}>
                  UI Meeting weekly - Week 15
                </Typography>
                <Typography
                  component="span"
                  sx={{ fontSize: '14px', color: theme.palette.darkGrey.main }}
                >
                  20 May 2022
                </Typography>
              </Stack>
            </Stack>
            <Stack>
              <RecordBtn />
            </Stack>
          </Stack>
        )}
        <Stack direction="row" alignItems="center" justifyContent="space-between" mb="16px">
          <Stack direction="row" alignItems="center" gap="20px">
            {!showMeetingHeader && <BackBtn />}
            <Button onClick={handleToggle} variant="contained" startIcon={<PeopleAltIcon />}>
              32
            </Button>
            <Button variant="contained" startIcon={<AddIcon />}>
              {showMeetingHeader && 'Thêm người'}
            </Button>
            <Button
              variant="outlined"
              startIcon={showMeetingHeader ? <VisibilityOffIcon /> : <VisibilityIcon />}
              onClick={() => setShowMeetingHeader(!showMeetingHeader)}
            >
              {showMeetingHeader && 'Thu gọn'}
            </Button>
            <TextField placeholder="Nhập tên phòng" onChange={handleChange} />
            <Button variant="text" onClick={handleClick} disabled={!value}>
              Tham gia
            </Button>
            <RecognitionProvider />
          </Stack>
          <Stack direction="row" gap="20px" alignItems="center">
            {!showMeetingHeader && <RecordBtn />}
            <Stack
              bgcolor={theme.palette.white.main}
              direction="row"
              alignItems="center"
              gap="7px"
              p="7px"
              borderRadius="7px"
            >
              <IconButton
                color={layout === 1 ? 'darkGrey' : 'lightGrey'}
                onClick={() => handleChangeLayout(1)}
              >
                <ViewModuleIcon />
              </IconButton>
              <IconButton
                color={layout === 2 ? 'darkGrey' : 'lightGrey'}
                onClick={() => handleChangeLayout(2)}
              >
                <ViewAgendaIcon />
              </IconButton>
              <IconButton
                color={layout === 3 ? 'darkGrey' : 'lightGrey'}
                onClick={() => handleChangeLayout(3)}
              >
                <ViewCompactIcon />
              </IconButton>
            </Stack>
          </Stack>
        </Stack>
        <GroupMeeting layout={layout} roomName={roomName} />
        <Stack mt="20px">
          {roomName && (
            <UserControls
              render={({
                hasAudio,
                isMuted,
                mute,
                unmute,
                isPaused,
                isSpeaking,
                isSpeakingWhileMuted,
                pauseVideo,
                resumeVideo,
                user,
                setDisplayName,
              }) => {
                return (
                  <LocalMediaControls
                    hasAudio={hasAudio}
                    isMuted={isMuted}
                    unmute={unmute}
                    mute={mute}
                    isPaused={isPaused}
                    resumeVideo={() => resumeVideo({ screenCapture: false })}
                    pauseVideo={() => pauseVideo({ screenCapture: false })}
                    isSpeaking={isSpeaking}
                    isSpeakingWhileMuted={isSpeakingWhileMuted}
                  />
                )
              }}
            />
          )}
        </Stack>
      </Stack>
      <Stack>
        <TabletLaptopDrawer
          drawer={drawer}
          anchor="right"
          variant="persistent"
          drawerWidth={drawerWidth}
          open={drawerOpen}
          sx={{
            display: { xs: 'block', sm: 'block' },
            '& .MuiDrawer-paper': {
              boxSizing: 'border-box',
              mt: downLg ? '0 !important' : '71px',
              height: downLg ? '100% !important' : 'calc(100% - 71px)',
              width: downLg ? '100%' : drawerWidth,
            },
          }}
        />
      </Stack>
    </Stack>
  )
}

Meeting.PageProps = {
  title: 'Meeting',
  showHeader: true,
}

export default Meeting

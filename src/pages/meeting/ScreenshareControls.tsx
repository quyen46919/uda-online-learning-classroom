import { RequestDisplayMedia } from '@andyet/simplewebrtc'
import { IconButton } from '@mui/material'
import { ShareScreenIcon } from 'libs/icons'
import { deviceSupportsVolumeMonitoring } from 'libs/utils/isMobile'

// ScreenShareControls displays a button that activates the screenshare flow.
// It also provides a link to install the screenshare extension if it is
// required by the user's browser.
const ScreenShareControls = () => (
  <RequestDisplayMedia
    audio
    volumeMonitoring={deviceSupportsVolumeMonitoring()}
    render={(getDisplayMedia, sharing) => {
      if (!sharing.available) {
        return <span>Empty space</span>
      }
      return (
        <IconButton color={!sharing.available ? 'white' : 'lightGrey'} onClick={getDisplayMedia}>
          <ShareScreenIcon />
        </IconButton>
      )
    }}
  />
)

export default ScreenShareControls

/* eslint-disable @typescript-eslint/no-unused-vars */
import {
  Actions,
  Connected,
  Connecting,
  LocalMediaList,
  Media,
  MediaControls,
  Provider,
  RemoteAudioPlayer,
  RequestUserMedia,
  Room,
  UserControls,
  Video,
} from '@andyet/simplewebrtc'
import { Box, CircularProgress, Stack, Typography, styled, useTheme } from '@mui/material'
import HiddenPeers from 'contexts/HiddenPeers'
import { PeopleAltIcon } from 'libs/icons'
import { useState } from 'react'
import { connect } from 'react-redux'
import PeerGrid from './PeerGrid'
import SharingScreenMedia from './SharingScreenMedia'

interface GroupMeetingProps {
  layout: number
  roomName?: string
}

interface LocalScreenProps {
  screenShareMedia: Media
}

interface Props {
  configUrl?: string
  userData?: string
  name?: string
  initialPassword?: string
  mute?: () => void
  unmute?: () => void
  layout: number
  roomName?: string
}

const GroupMeeting = (props: GroupMeetingProps) => {
  const { layout, roomName } = props
  const [hiddenPeers, setHiddenPeers] = useState<string[]>([])
  const [activeSpeakerView, setActiveSpeakerView] = useState<boolean>(false)
  const theme = useTheme()

  const StyledVideo = styled(Video)({
    transform: 'none !important',
    width: '100%',
    height: '100%',
    borderRadius: '25px',
    objectFit: 'cover',
  })

  const API_KEY = '98dcbe82b7b423aa36d6951d'
  const ROOM_PASSWORD = 'quan'
  const CONFIG_URL = `https://api.simplewebrtc.com/config/guest/${API_KEY}`

  const LocalScreen = ({ screenShareMedia }: LocalScreenProps) => {
    return (
      <MediaControls
        media={screenShareMedia}
        autoRemove={true}
        render={({ media, stopSharing }) => <>{media && <StyledVideo media={media} />}</>}
      />
    )
  }

  const togglePeer = (peerId: string) => {
    if (hiddenPeers.includes(peerId)) {
      const otherHiddenPeers = hiddenPeers.filter((id) => id !== peerId)
      setHiddenPeers(otherHiddenPeers)
    } else {
      setHiddenPeers([...hiddenPeers, peerId])
    }
  }

  if (!roomName) {
    return <span>There's no room available.</span>
  } else {
    return (
      <Box>
        <Provider configUrl={CONFIG_URL}>
          <RemoteAudioPlayer />
          <HiddenPeers.Provider
            value={{
              hiddenPeers: hiddenPeers,
              togglePeer: togglePeer,
            }}
          >
            <Connecting>
              <CircularProgress />
            </Connecting>

            <Connected>
              <RequestUserMedia
                audio
                video={{
                  width: {
                    min: 320,
                    max: 320,
                  },
                  height: {
                    min: 320,
                    max: 320,
                  },
                }}
                auto
              />

              <Room name={roomName ? roomName : ''} password={ROOM_PASSWORD}>
                {(props) => {
                  return (
                    <Box
                      sx={{
                        display: 'grid',
                        position: 'relative',
                        gridTemplateColumns: 'repeat(auto-fill, minmax(320px, 1fr))',
                        gap: '20px',
                        '& video': {
                          width: '100%',
                          height: '100%',
                        },
                        ...(layout === 2 && {
                          '& > video:first-of-type': {
                            gridColumn: '1 / -2',
                            gridRow: '1 / span 3',
                            height: '100%',
                          },
                        }),
                        ...(layout === 3 && {
                          '& > video:first-of-type': {
                            gridColumn: '1 / -1',
                            maxHeight: 'calc(100vh - 140px)',
                          },
                        }),
                      }}
                    >
                      <UserControls
                        render={({ ...props }) => {
                          return (
                            <>
                              <LocalMediaList
                                shared={true}
                                render={({ media }) => {
                                  const videos = media.filter((m) => m.kind === 'video')
                                  if (videos.length > 0) {
                                    return (
                                      <>
                                        {videos.map((m) =>
                                          m.screenCapture ? (
                                            <LocalScreen key={m.id} screenShareMedia={m} />
                                          ) : (
                                            <StyledVideo key={m.id} media={m} />
                                          ),
                                        )}
                                      </>
                                    )
                                  }
                                  return <span>Empty video</span>
                                }}
                              />
                              <Stack
                                position="absolute"
                                top={0}
                                left={0}
                                zIndex={10}
                                width="fit-content"
                                height="100%"
                                p="15px"
                                justifyContent="space-between"
                              >
                                <Stack
                                  direction="row"
                                  alignItems="center"
                                  gap="10px"
                                  width="fit-content"
                                  p="8px"
                                  borderRadius="15px"
                                  sx={{
                                    backgroundColor: 'rgb(200 196 196 / 30%)',
                                    color: theme.palette.white.main,
                                  }}
                                >
                                  <PeopleAltIcon />
                                  <Typography component="span">{props.user.displayName}</Typography>
                                </Stack>
                              </Stack>
                            </>
                          )
                        }}
                      />
                      {props.joined && <SharingScreenMedia media={props.remoteMedia} />}
                      {props.joined && (
                        <PeerGrid
                          roomAddress={props.room?.address ? props.room.address : ''}
                          activeSpeakerView={activeSpeakerView}
                          // setPassword={setPassword}
                        />
                      )}
                    </Box>
                  )
                }}
              </Room>
            </Connected>
          </HiddenPeers.Provider>
        </Provider>
      </Box>
    )
  }
}

function mapDispatchToProps(dispatch: Function, props: Props) {
  return {
    ...props,
    mute: () => dispatch(Actions.muteSelf()),
    unmute: () => dispatch(Actions.unmuteSelf()),
  }
}

export default connect(null, mapDispatchToProps)(GroupMeeting)

import { Media, Video } from '@andyet/simplewebrtc'
import { styled } from '@mui/material'

interface LoadingVideoProps {
  media: Media
  qualityProfile?: 'high' | 'medium' | 'low'
}

interface PeerGridItemMediaProps {
  media: Media[]
}

const StyledVideo = styled(Video)({
  transform: 'none !important',
  width: '100%',
  height: '100%',
  borderRadius: '25px',
  objectFit: 'cover',
})

const LoadingVideo = (props: LoadingVideoProps) => {
  if (!props.media.loaded) {
    return <span>AudioOnlyPeer</span>
  }
  return (
    <StyledVideo
      media={props.media}
      qualityProfile={props.media.screenCapture ? undefined : props.qualityProfile}
    />
  )
}

const SharingScreenMedia = ({ media }: PeerGridItemMediaProps) => {
  const videoStreams = media.filter((m) => m.kind === 'video' && !m.remoteDisabled)
  if (videoStreams.length > 0) {
    const screenCaptureStreams = videoStreams.filter((s) => s.screenCapture)
    if (screenCaptureStreams.length === 1) {
      return <LoadingVideo media={screenCaptureStreams[0]} />
    }
  }
  return <></>
}

export default SharingScreenMedia

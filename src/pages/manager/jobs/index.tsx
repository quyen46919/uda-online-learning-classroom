import React from 'react'
import GroupNoteBoardPage from './GroupNoteBoardPage'
import ManagerLayout from 'pages/manager'

const Jobs = () => {
  return (
    <ManagerLayout>
      <GroupNoteBoardPage />
    </ManagerLayout>
  )
}

Jobs.PageProps = {
  title: 'Công việc',
  showHeader: true,
}

export default Jobs

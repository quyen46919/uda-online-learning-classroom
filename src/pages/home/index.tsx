import { Box } from '@mui/material'
import React from 'react'

function HomePage() {
  return <Box minHeight="50vh">this is homepage</Box>
}

HomePage.PageProps = {
  title: 'Trang chủ',
  showHeader: true,
  showFooter: true,
}

export default HomePage
